
/* Account Testdaten Password klartext "abc123" */
INSERT INTO account(email, password)
VALUES('max@web.de''$2a$06$z.bmQIe.cYNUnIpJ/tV0Z.TVdcMl6VsyKY6ZGadaiuEuf8FET8/vW')


INSERT INTO app_user_user_profile (user_id, user_profile_id)
  SELECT account.email, profile.id FROM account account, user_profile_role profile
  where account.email='max@web.de' and profile.role='USER';


/* Profile Rollen Typen */

INSERT INTO user_profile_role(role)
VALUES ('USER');

INSERT INTO user_profile_role(role)
VALUES ('ADMIN');

INSERT INTO user_profile_role(role)
VALUES ('DBA');
COMMIT;


INSERT INTO user_profile(first_name, last_name, user_name, birthday)
VALUES ('Max','Mustermann','MegaMax', '1990-10-02');


INSERT INTO address(city, country, postal_code, street)
VALUES ('Magdeburg','Deutschland','39104','Musterstrasse 5');


INSERT INTO address(city, country, postal_code, street)
VALUES ('Hamburg','Deutschland','89542','Speicherstadtstrass 2');


INSERT INTO location(loc_name, loc_description, sea, street)
VALUES ('Hamburg','Deutschland','89542','Speicherstadtstrass 2');




