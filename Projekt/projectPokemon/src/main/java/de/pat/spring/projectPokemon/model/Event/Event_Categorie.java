package de.pat.spring.projectPokemon.model.Event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table(name = "event_categorie")
public class Event_Categorie implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "event_categorie_id")
    private Integer event_categorie_id;


    @Column(name = "event_cat_description" , unique = true, nullable = false)
    private String event_cat_description;

    public Event_Categorie() {
    }

    //region Getter & Setter
    public Integer getEvent_categorie_id() {
        return event_categorie_id;
    }

    public String getEvent_cat_description() {
        return event_cat_description;
    }

    public void setEvent_cat_description(String event_cat_description) {
        this.event_cat_description = event_cat_description;
    }
    //endregion

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
        return true;

        if (obj== null || getClass() != obj.getClass())
            return false;

        Event_Categorie that = (Event_Categorie) obj;

        if (event_cat_description != null ? !event_cat_description.equals(that.event_cat_description) : that.event_cat_description!=null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        return event_cat_description !=null ? event_cat_description.hashCode():0;
    }


    @Override
    public String toString() {
        return event_cat_description;
    }
}
