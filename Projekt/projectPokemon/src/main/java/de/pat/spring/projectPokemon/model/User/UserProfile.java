package de.pat.spring.projectPokemon.model.User;

import de.pat.spring.projectPokemon.model.Address;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by schwirzke on 06.10.16.
 */

@Entity
@Table(name="user_profile")
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer user_id;

    @Column(name = "user_name", nullable = false)
    private String username;

    @Column(name="first_name", nullable=false)
    private String firstName;

    @Column(name="last_name", nullable=false)
    private String lastName;

    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn (name = "address_id")
    private Address address;

    public UserProfile() {
    }

    //region Getter & Setter
    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    //endregion


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProfile userProfile = (UserProfile) o;

        if (getUser_id() != null ? !getUser_id().equals(userProfile.getUser_id()) : userProfile.getUser_id() != null) return false;
        if (getUsername() != null ? !getUsername().equals(userProfile.getUsername()) : userProfile.getUsername() != null)
            return false;
        if (getFirstName() != null ? !getFirstName().equals(userProfile.getFirstName()) : userProfile.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(userProfile.getLastName()) : userProfile.getLastName() != null)
            return false;
        if (getBirthday() != null ? !getBirthday().equals(userProfile.getBirthday()) : userProfile.getBirthday() != null)
            return false;
        return getAddress() != null ? getAddress().equals(userProfile.getAddress()) : userProfile.getAddress() == null;

    }

    @Override
    public int hashCode() {
        int result = getUser_id() != null ? getUser_id().hashCode() : 0;
        result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getBirthday() != null ? getBirthday().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        return result;
    }
}
