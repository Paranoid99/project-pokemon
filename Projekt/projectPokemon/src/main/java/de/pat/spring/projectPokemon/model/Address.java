package de.pat.spring.projectPokemon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by schwirzke on 06.10.16.
 */

@Entity
@Table(name = "address")
public class Address implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id", nullable = false, updatable = false)
    private Integer address_id;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "postal_code", nullable = false)
    private String postal_code;

    @Column(name = "country", nullable = false)
    private String country;

    public Address() {
    }

    //region Getter & Setter
    public Integer getAddress_id() {
        return address_id;
    }

    public void setAddress_id(Integer address_id) {
        this.address_id = address_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    //endregion


    @Override
    public String toString() {

        return String.format("[%s] (%d, %s, {%s}, %s, %s)",
                this.getClass().getSimpleName(),
                address_id,
                street,
                city,
                postal_code,
                country
        );
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return address_id != null ? address_id.equals(address.address_id) : address.address_id == null;

    }


    @Override
    public int hashCode() {

        return address_id != null ? address_id.hashCode() : 0;
    }
}
