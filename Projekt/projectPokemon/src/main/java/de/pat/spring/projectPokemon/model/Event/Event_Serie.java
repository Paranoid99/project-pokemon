package de.pat.spring.projectPokemon.model.Event;

import de.pat.spring.projectPokemon.model.Location.Location;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table(name = "event_serie")
public class Event_Serie implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer event_serie_id;

    @ManyToOne
    private Event event;

    @ManyToOne
    private Location location;

    public Event_Serie() {
    }

    //region Getter & Setter
    public Integer getEvent_serie_id() {
        return event_serie_id;
    }

    public void setEvent_serie_id(Integer event_serie_id) {
        this.event_serie_id = event_serie_id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    //endregion


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Event_Serie event_serie = (Event_Serie) o;


        if (event != null ? !event.equals(event_serie.event) : event_serie.event != null)
            return false;

        if (location != null ? !location.equals(event_serie.location) : event_serie.location != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = event != null ? event.hashCode() : 0;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }
}
