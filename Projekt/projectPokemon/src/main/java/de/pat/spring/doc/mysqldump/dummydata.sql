
INSERT INTO account
VALUES ('max@web.de','$2a$06$z.bmQIe.cYNUnIpJ/tV0Z.TVdcMl6VsyKY6ZGadaiuEuf8FET8/vW',1);

INSERT INTO address
VALUES  (1,'Magdeburg','Deutschland','39104','Musterstrasse 5'),
        (2,'Hamburg','Deutschland','89542','Speicherstadtstrass 2');

INSERT INTO app_user_user_profile`
VALUES ('max@web.de',1);

INSERT INTO location
VALUES (1,'Betreten Sie das Theater und genießen Sie Momente, die Sie nie vergessen werden. Neben der zentralen Hafenlage bietet das außergewöhnliche Gebäude einen modernen Theatersaal mit 2.030 Sitzplätzen, ein offenes Foyer über 3 Ebenen und eine Sammlung zeitgenössischer Kunst. Das exklusiv ausgestattete \"Skyline Restaurant\" bietet einen einzigartigen Ausblick auf Hamburgs Panorama. Die Kombination aus modernem Ambiente und maritimer Atmosphäre bildet ein vollkommenes Umfeld für Events aller Art','Stage Theater im Hafen',500,2);

INSERT INTO user_profile
VALUES (1,'1990-10-02','Max','Mustermann','MegaMax',1);

INSERT INTO user_profile_role
VALUES  (2,'ADMIN'),
        (3,'DBA'),
        (1,'USER');

