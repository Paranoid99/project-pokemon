package de.pat.spring.projectPokemon.dao;

import de.pat.spring.projectPokemon.model.User.Account;
import org.springframework.stereotype.Repository;


@Repository("userDao")
public class UserDaoImpl extends BasicDaoAbstract <Account>{

    public UserDaoImpl(){

        setClazz(Account.class);
    }

}
