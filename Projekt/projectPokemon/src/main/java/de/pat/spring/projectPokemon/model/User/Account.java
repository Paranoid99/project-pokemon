package de.pat.spring.projectPokemon.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.validator.constraints.Email;

/**
 * Created by schwirzke on 09.09.16.
 */
@Entity
@Table(name="account")
public class Account implements Serializable{

    @Id
    @Column(name="email", nullable=false, unique = true)
    @Email
    private String email;

    @Column(name="password", nullable=false)
    private String password;

    @OneToOne
    private UserProfile userProfile;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "app_user_user_profile",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_profile_id") })
    private Set<UserProfileRole> userProfileRoles = new HashSet<UserProfileRole>();

    //region Getter & Setter

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //endregion

    public Set<UserProfileRole> getUserProfileRoles() {
        return userProfileRoles;
    }

    public void setUserProfileRoles(Set<UserProfileRole> userProfileRoles) {
        this.userProfileRoles = userProfileRoles;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Account))
            return false;
        Account other = (Account) obj;

        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return  "email=" + email + ", " +
                "password=" + password  +
                ", userProfileRoles=" + userProfileRoles +"]";
    }


}
