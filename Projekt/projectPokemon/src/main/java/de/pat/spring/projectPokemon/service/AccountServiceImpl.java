package de.pat.spring.projectPokemon.service;

import de.pat.spring.projectPokemon.dao.BasicDaoAbstract;
import de.pat.spring.projectPokemon.model.User.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private BasicDaoAbstract<Account> userBasicDaoAbstract;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void update(Account entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userBasicDaoAbstract.update(entity);

    }

    @Override
    public Account findByRestriction(String restriction, Object value) {
        return userBasicDaoAbstract.getByRestriction(restriction, value);
    }
}
