package de.pat.spring.projectPokemon.model.User;

import java.io.Serializable;

/**
 * Created by schwirzke on 09.09.16.
 */
public enum UserProfileRoleEnum implements Serializable{

    USER("USER"),
    DBA("DBA"),
    ADMIN("ADMIN");

    String userProfileRole;

    UserProfileRoleEnum(String userProfileRole){
        this.userProfileRole = userProfileRole;
    }

    public String getUserProfileRole(){
        return userProfileRole;
    }
}
