package de.pat.spring.projectPokemon.service;

import de.pat.spring.projectPokemon.model.User.Account;
import de.pat.spring.projectPokemon.model.User.UserProfileRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountService accountService;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String em) throws UsernameNotFoundException {

        Account account = accountService.findByRestriction("email", em);

        if (account == null) {

            throw new UsernameNotFoundException(String.format("account not found: %s", em));
        }

        return new org.springframework.security.core.userdetails.User(
                account.getEmail(),
                account.getPassword(),
                true,
                true,
                true,
                true,
                getGrantedAuthorities(account)
        );
    }


    private List<GrantedAuthority> getGrantedAuthorities(Account account) {

        List<GrantedAuthority> authorities = new ArrayList<>();

        for (UserProfileRole userProfileRole : account.getUserProfileRoles()) {

            authorities.add(new SimpleGrantedAuthority("ROLE_" + userProfileRole.getRole()));
        }

        return authorities;
    }
}
