package de.pat.spring.projectPokemon.model.Location;

/**
 * Created by schwirzke on 06.10.16.
 */


import de.pat.spring.projectPokemon.model.Address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="location")
public class Location  implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "loc_id")
    private Integer location_id;

    @Column(name = "loc_name")
    private String location_name;

    @Column(name = "loc_description", length = 500)
    private String location_description;

    @Column(name = "seat_capacity" )
    private Integer seat_capacity;

    @OneToOne
    private Address address;

    public Location() {
    }

    //region Getter & Setter
    public Integer getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Integer location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLocation_description() {
        return location_description;
    }

    public void setLocation_description(String location_description) {
        this.location_description = location_description;
    }

    public Integer getSeat_capacity() {
        return seat_capacity;
    }

    public void setSeat_capacity(Integer seat_capacity) {
        this.seat_capacity = seat_capacity;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    //endregion

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Location location = (Location) o;

        if (location_id != null ? !location_id.equals(location.location_id) : location.location_id != null)
            return false;
        if (location_name != null ? !location_name.equals(location.location_name) : location.location_name != null)
            return false;
        if (location_description != null ? !location_description.equals(location.location_description) : location.location_description != null)
            return false;
        if (seat_capacity != null ? !seat_capacity.equals(location.seat_capacity) : location.seat_capacity != null)
            return false;
        return address != null ? address.equals(location.address) : location.address == null;

    }

    @Override
    public int hashCode() {
        int result = location_id != null ? location_id.hashCode() : 0;
        result = 31 * result + (location_name != null ? location_name.hashCode() : 0);
        result = 31 * result + (location_description != null ? location_description.hashCode() : 0);
        result = 31 * result + (seat_capacity != null ? seat_capacity.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}
