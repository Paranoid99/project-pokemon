#Ticket Pokémon

##Zielsetzung

`Ticket Pokémon` ist eine Onlineverkaufsplattform für Veranstaltungstickets. 

Veranstalter können u.a.:

  * Veranstaltungsorte anlegen
    * Name
    * Bild
    * Beschreibung
    * Bereiche
      * Anzahl Reihen
      * Anzahl Plätze pro Reihe

  * Events anlegen
    * Name
    * Bild
    * Beschreibung
    * Art der Veranstaltung
    
Benutzer der Verkaufsplattform können u.a.:
  
  * Tickets für ein Event in verschiedenen Kategorien kaufen
  * Käufe stornieren

"Externe" Systeme können:

  * eine Liste der aktuellen Events über einen RESTful Service abrufen (JSON)


Die Applikation `Ticket Pokémon` ist als Spring MVC Anwendung als *Einzelprojekt* zu realisieren. 
Dabei sollen u.a. folgende Technologien eingesetzt werden:

  * JPA, Hibernate
  * JSP (Alternativen sind erlaubt)
  * jQuery, Bootstrap
  * HTML5
  * JAX-RS

##Spezifikation

Das System ist unter Verwendung geeigneter UML-Diagrammarten zu modellieren, u.a.: 

  * Komponentendiagramm
  * Use-Case-Diagramm
  * Aktivitätsdiagramm
  * Sequenzdiagramm  

##Akteure Fachliche Anforderungen

####Veranstalter muss
  * sich registrieren können
  * sich anmelden können
  * einen Veranstaltungsort verwalten können
  * eine Veranstaltungsserie (Mehrere Event) verwalten können
  * eine einzelne Veranstaltung verwalten können, ggfs. mit Bezug auf eine Serie
  * Ticketkategorien und Preise festlegen können
  * einsehen der laufenden Verkäufe/ Verkaufszahlen
    
####Kunde muss
  * sich registrieren können
  * sich anmelden können
  * sein Kundenkonto verwalten können
  * Tickets kaufen können, ggfs mit Platzreservierung, ggfs mit verschieden Zahlarten
  * Käufe stornieren können (Stornierungscode, via Kundenkonto)

####Externes System
  * können über Restful Services einzelne Veranstaltungsdaten 
    oder Listen von Veranstalter abfragen

####Admin
  * kann bei Problemen mit dem Benutzerkonto eingreifen

####Veranstalter und Kunde
 * Veranstaltungen müssen nach verscheidenen Kriterien gesucht werden können