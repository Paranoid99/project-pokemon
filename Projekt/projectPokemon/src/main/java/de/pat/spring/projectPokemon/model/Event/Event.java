package de.pat.spring.projectPokemon.model.Event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by schwirzke on 07.10.16.
 */
@Entity
@Table(name = "event")
public class Event implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer event_id;

    @Column(name = "event_name", unique = true, nullable = false)
    private String event_name;

    @Column(name = "event_description")
    private String event_description;

    @Temporal(TemporalType.TIME)
    private Date event_start_time;

    @ManyToOne
    private Event_Categorie event_categorie;

    public Event() {
    }

    //region Getter & Setter
    public Integer getEvent_id() {
        return event_id;
    }

    public void setEvent_id(Integer event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_description() {
        return event_description;
    }

    public void setEvent_description(String event_description) {
        this.event_description = event_description;
    }

    public Date getEvent_start_time() {
        return event_start_time;
    }

    public void setEvent_start_time(Date event_start_time) {
        this.event_start_time = event_start_time;
    }

    public Event_Categorie getEvent_categorie() {
        return event_categorie;
    }

    public void setEvent_categorie(Event_Categorie event_categorie) {
        this.event_categorie = event_categorie;
    }

    //endregion

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Event event = (Event) o;

        return event_id != null ? event_id.equals(event.event_id) : event.event_id == null;

    }


    @Override
    public int hashCode() {

        return event_id != null ? event_id.hashCode() : 0;
    }

    @Override
    public String toString() {

        return String.format("[%s] (%d, %s,%s )",
                this.getClass().getSimpleName(),
                event_id,
                event_name,
                event_description

        );
    }
}
