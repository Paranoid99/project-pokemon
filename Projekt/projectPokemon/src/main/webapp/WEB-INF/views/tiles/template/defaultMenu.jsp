<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Titel und Schalter werden für eine bessere mobile Ansicht zusammengefasst -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Navigation ein-/ausblenden</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${pageContext.request.contextPath}/">HOME</a>
		</div>

		<!-- Alle Navigationslinks, Formulare und anderer Inhalt werden hier zusammengefasst und können dann ein- und ausgeblendet werden -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a id="music" href="${pageContext.request.contextPath}/">MUSIC</a></li>
				<li><a id="sport" href="${pageContext.request.contextPath}/">SPORTS </a></li>


				<li  class="dropdown-toggle">
					<a id="arts" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTS & THEATER <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${pageContext.request.contextPath}/">BALLET/DANCE</a></li>
						<li><a href="${pageContext.request.contextPath}/">OPERA</a></li>
						<li><a href="${pageContext.request.contextPath}/">COMEDY</a></li>
					</ul>
				</li>
				<li><a id="shop" href="${pageContext.request.contextPath}/shop">MERCHANDISE</a></li>
			</ul>
			<form class="navbar-form navbar-right" role="search">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Suchen">
				</div>
				<button type="submit" class="btn btn-default">Los</button>
			</form>

			<ul class="nav navbar-nav navbar-right">
				<li><p class="navbar-text"></p></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
					<ul id="login-dp" class="dropdown-menu">
						<li>
							<div class="row">
								<div class="col-md-12">
								<sec:authorize access="isAnonymous()">
									Login via
									<div class="social-buttons">
										<a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
										<a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
									</div>
									oder
								</sec:authorize>
									<c:url var="loginUrl" value="/login"/>

									<form class="form" role="form" method="post" action="${loginUrl}" accept-charset="UTF-8" id="login-nav">

										<sec:authorize access="isAnonymous()">
										<div class="input-group input-sm">
											<label class="sr-only" for="account">Email</label>
											<input type="text" class="form-control" id="account" name="email" placeholder="Email" req>
										</div>

										<div class="input-group input-sm">
											<label class="sr-only" for="pass">Passwort</label>
											<input type="password" class="form-control" id="pass" name="password" placeholder="Password">
											<div class="help-block text-right"><a href="">Passwort vergessen ?</a></div>
										</div>
										</sec:authorize>

										<!--To DO-->
										<div class="input-group input-group-sm">
											<sec:authorize access="isAnonymous()">
												<button type="submit" class="btn btn-primary btn-block">Login</button>
											</sec:authorize>

											<sec:authorize access="isAuthenticated()">
												<button type="submit" onclick= "href='${pageContext.request.contextPath}/logout'" class="btn btn-primary btn-block">Logout</button>
											</sec:authorize>
										</div>
										<sec:authorize access="isAnonymous()">
										<div class="checkbox">
											<label>
												<input type="checkbox"> Eingeloggt bleiben
											</label>
										</div>
										</sec:authorize>
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									</form>
								</div>
								<sec:authorize access="isAnonymous()">
								<div class="bottom text-center">
									Neu hier ? <a href="${pageContext.request.contextPath}/registerUser"><b>Registrieren</b></a>
								</div>
								</sec:authorize>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>

