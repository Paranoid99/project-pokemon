<!--Vorname-->
<div class="form-group">
    <label for="name" class="cols-sm-2 control-label">Vorname</label>
    <div class="cols-sm-10">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-account fa" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="first_name" id="first_name"  placeholder="Bitte Vornamen eingeben"/>
        </div>
    </div>
</div>
<!--Nachname-->
<div class="form-group">
    <label for="name" class="cols-sm-2 control-label">Nachname</label>
    <div class="cols-sm-10">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-account fa" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="name" id="name"  placeholder="Bitte Nachnamen eingeben"/>
        </div>
    </div>
</div>

<!--Username-->
<div class="form-group">
    <label for="username" class="cols-sm-2 control-label">Benutzername</label>
    <div class="cols-sm-10">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="username" id="username"  placeholder="Wählen sie einen Benutzernamen aus"/>
        </div>
    </div>
</div>